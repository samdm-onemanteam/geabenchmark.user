# Check on wich computer I am
if (grepl("octacore2", Sys.info()["nodename"])) {
  baseDir <- "/scratch/demeyer_sam/GEABenchmark-package/geabenchmark.user"
} else if (grepl("archlinux", Sys.info()["nodename"])) {
  baseDir <- paste0("/storage/Dropbox/2_Documenten/1_UGent/5_Uma2/2S-thesis/",
                    "GEABenchmark-package/GEABenchmark.user")
} else {
  stop("Where am I?")
}
source(file.path(baseDir, "benchmark-setup.R"))
bmSetup(baseDir)

# ------------------------------------------------------------------------------
# Set benchmark options
# ------------------------------------------------------------------------------

library("GEABenchmark")

minsize <- 10
maxsize <- 600
mgc     <- 0.0

GSMethodName <- paste0("Chris", minsize, "_", maxsize)
GSMethodFun  <- function(mappedTopTable) {
    extractGeneSet(mappedTopTable, 0.05, minSize = minsize, maxSize = maxsize)
}
ClusterName  <- paste0("MGclus_FC", gsub("\\.", "_", mgc))
ClusterFun <- function(dataEntry, geneSet) {
  sg <- induced_subgraph(
    graph = FA_network$netw_top_igraph,
    v     = V(FA_network$netw_top_igraph)[name %in% geneSet]
  )
  MGclus(sg, edgeWeightName = "PFC", delta = mgc)
}
prefix <- paste(GSMethodName, ClusterName, sep = "-")

benchmark <- GEABenchmark(
  dataDir              = file.path(baseDir, "dataDir"),
  idType               = "ENSEMBL",
  idMapper             = plug("FunCoup", idMapper_netw),
  tableMapper          = plug("lowestPval", lowestPval),
  geneSetMethod        = plug(GSMethodName, GSMethodFun),
  geneSetClusterMethod = plug(ClusterName, ClusterFun)
)

# ------------------------------------------------------------------------------
# True positives
# ------------------------------------------------------------------------------

goodDataEntries  <- getDataEntries(benchmark, goodExperiments)
queryGeneSets    <- getGeneSets(benchmark, goodDataEntries)
queryDatasets    <- getDatasets(benchmark, goodDataEntries)
queryGeneSets_cl <- getClusteredGeneSets(benchmark, goodDataEntries, ncores = 0)

TPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3),
                              queries = queryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Fisher_cl"      = prepTool(fun     = getFisherTool(23e3, clustered = TRUE),
                              queries = queryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3),
                              queries = queryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease_cl"        = prepTool(fun     = getEaseTool(23e3, clustered = TRUE),
                              queries = queryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Padog"          = prepTool(fun     = getPadogTool(
                                benchmark,
                                padog_parallel = TRUE,
                                padog_ncores = 6
                              ),
                              queries = queryDatasets,
                              targets = targetGeneSets,
                              prefix  = "Tarca")
  ,
  "CrossTalkB"     = prepTool(fun     = getCBplugin(randNetPath, 5),
                              queries = queryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "CrossTalkB_cl"  = prepTool(fun     = getCBplugin(
                                randNetPath, 5,
                                clustered = TRUE,
                                saveAs = paste0("dataDir/Extra/CBoutput/CB_cl_",
                                                prefix, ".tsv")
                              ),
                              queries = queryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
)

runTPBenchmark(benchmark, TPsetup, goodDataEntries)
TPsummary <- rbind(summarizeTPResults(benchmark, prefix,
                                      dataEntries = goodDataEntries),
                   summarizeTPResults(benchmark, "Tarca", toolNames = "Padog",
                                      dataEntries = goodDataEntries))
tpp <- TPplot_pretty(
  TPsummary,
  paste0("Using only datasets with at least 10 significantly DE genes\n",
         "Ensembl id's are mapped trough FunCoup, ",
         "top table mapping as in Tarca et al.\n",
         "Gene set sizes limited between ", minsize, " and ", maxsize,
         ". MGclus delta = ", mgc)
)
print(tpp)

# ------------------------------------------------------------------------------
# True negatives, using label swaps
# ------------------------------------------------------------------------------

settings(benchmark) <- list(
  geneSetMethod = plug("MSigDbSizes", extractGSwithMSigDBsizes)
)

goodDataEntries <- getDataEntries(benchmark, goodExperiments)
pQueryGeneSets <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "GeneSets", ncores = 0
)
pQueryDatasets <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "Datasets"
)
pQueryGeneSets_cl <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "ClusteredGeneSets", ncores = 0
)

FPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Fisher_cl"      = prepTool(fun     = getFisherTool(23e3, clustered = TRUE),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease_cl"        = prepTool(fun     = getEaseTool(23e3, clustered = TRUE),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Padog"          = prepTool(fun     = getPadogTool(
                                benchmark,
                                padog_parallel = TRUE,
                                padog_ncores = 6
                              ),
                              queries = pQueryDatasets,
                              targets = targetGeneSets,
                              prefix  = "Tarca")
  ,
  "CrossTalkB"     = prepTool(fun     = getCBplugin(randNetPath, 5),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "CrossTalkB_cl"  = prepTool(fun     = getCBplugin(
                                randNetPath, 5,
                                clustered = TRUE
                              ),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
)

runFPBenchmark(benchmark, FPsetup, goodDataEntries)
FPsummary <- rbind(summarizeFPResults(benchmark, prefix,
                                      dataEntries = goodDataEntries),
                   summarizeFPResults(benchmark, "Tarca", toolNames = "Padog",
                                      dataEntries = goodDataEntries))
fpp <- FPplot_pretty(
  FPsummary,
  paste0("Using only datasets with at least 10 significantly DE genes\n",
         "Ensembl id's are mapped trough FunCoup, ",
         "top table mapping as in Tarca et al.\n",
         "Gene set sizes reflect MSigDB gene set sizes. ",
         "MGclus delta = ", mgc)
)
print(fpp)

# ------------------------------------------------------------------------------
# True negatives, using gene set randomisation
# ------------------------------------------------------------------------------

prefix <- paste0(prefix, "-FCrand")
goodDataEntries <- getDataEntries(benchmark, goodExperiments)

setRandomizer <- create.randomizeGeneSet(FA_network$netw_igraph, error = 0.05)
pQueryGeneSets <- getPermutations(benchmark, goodDataEntries,
                                  permutations = 1:10, what = setRandomizer,
                                  recompute = F, ncores = 3)
pQueryGeneSets_cl <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "ClusteredGeneSets",
  recompute = F, ncores = 4
)

FPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Fisher_cl"      = prepTool(fun     = getFisherTool(23e3, clustered = TRUE),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease_cl"        = prepTool(fun     = getEaseTool(23e3, clustered = TRUE),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "CrossTalkB"     = prepTool(fun     = getCBplugin(randNetPath, 5),
                              queries = pQueryGeneSets,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "CrossTalkB_cl"  = prepTool(fun     = getCBplugin(
                                randNetPath, 5,
                                clustered = TRUE
                              ),
                              queries = pQueryGeneSets_cl,
                              targets = targetGeneSets,
                              prefix  = prefix)
)

runFPBenchmark(benchmark, FPsetup, goodDataEntries, ncores = 1)
FPsummary <- summarizeFPResults(benchmark, prefix,
                                toolNames = names(FPsetup),
                                saveAs = paste0(prefix,"_MSigDbVsKeGG_fullFDR.FP"),
                                dataEntries = goodDataEntries)

fpp <- FPplot_pretty(FPsummary, "Test", 12)
fpp

if (F) {
  plot_dir <- file.path(baseDir, "plots")
  dir.create(plot_dir)

  ggsave(file.path(plot_dir, paste0(prefix, "FP.pdf")), plot = fpp)
}

# ------------------------------------------------------------------------------
# Save the plots
# ------------------------------------------------------------------------------

if (F) {
  plot_dir <- file.path(baseDir, "plots")
  dir.create(plot_dir)

  save_plot(file.path(plot_dir, paste0("cl-", prefix, ".TP.pdf")), tpp, base_width = 7)
  save_plot(file.path(plot_dir, paste0("cl-", prefix, ".FP.pdf")), fpp, base_width = 7, base_height = 7)
}
