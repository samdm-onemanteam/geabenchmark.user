# Check on wich computer I am
if (grepl("octacore2", Sys.info()["nodename"])) {
  baseDir <- "/scratch/demeyer_sam/GEABenchmark-package/geabenchmark.user"
} else if (grepl("archlinux", Sys.info()["nodename"])) {
  baseDir <- paste0("/storage/Dropbox/2_Documenten/1_UGent/5_Uma2/2S-thesis/",
                    "GEABenchmark-package/GEABenchmark.user")
} else {
  stop("Where am I?")
}
plot_dir <- file.path(baseDir, "plots")

# ------------------------------------------------------------------------------
# Set benchmark options
# ------------------------------------------------------------------------------

library("GEABenchmark")
library("readr")
library("igraph")
library("foreach")

# load tools
source(file.path(baseDir, "benchmark-setup.R"))
bmSetup(baseDir, "dataDir.negative-geneperm")

# load funcoup in memory
FunCoup <- read_tsv(file.path(baseDir, "dataDir.negative-geneperm/Extra/FC3.0_H.sapiens_compact_correctformat.tsv"))
colnames(FunCoup) <- c("Gene1", "Gene2", "PFC")
FunCoup_g <- graph_from_data_frame(FunCoup)

# load KEGG pathways in memory
KEGG <- read_tsv(file.path(
  baseDir,
  "dataDir.negative-geneperm/TargetGeneSets/KEGG.tsv"
))
targetGeneSets <- lapply(by(KEGG$GENE_ID, KEGG["KEGG_ID"], as.character), identity)

# prepare gene set selection method
gsm_qvalcut <- 0.01
gsm_foldchc <- 1.50
gsm_minfrac <- 0.01
gsm_abslmax <- Inf

gsm_methodName <- paste0("qvalcut", gsub("\\.","_", gsm_qvalcut),
                         "foldchc", gsub("\\.","_", gsm_foldchc),
                         "minfrac", gsub("\\.","_", gsm_minfrac),
                         "abslmax", gsub("\\.","_", gsm_abslmax))
gsm_method <- function(toptable) {
  importantGenes <- subset(toptable, adj.P.Val <= gsm_qvalcut &
                           abs(logFC) >= log2(gsm_foldchc))
  minsize <- gsm_minfrac * nrow(toptable)
  if (nrow(importantGenes) < minsize) {
    return(toptable[1:minsize, "MAPPING"])
  } else if (nrow(importantGenes) > gsm_abslmax) {
    return(toptable[1:gsm_abslmax, "MAPPING"])
  } else {
    return(importantGenes$MAPPING)
  }
}

# prepare clustering algorithms
# MGclus
mgc_cutoff <- 0.0
mgc_ClusterFunName <- paste0("MGclus", gsub("\\.", "_", mgc_cutoff))
mgc_ClusterFun_ <- prepare.MGclus.ClusterFun(FunCoup_g,
                                             edgeWeightName = "PFC",
                                             delta = mgc_cutoff,
                                             verbose = T)
mgc_ClusterFun <- function(dataEntry, geneSet) {
  mgc_ClusterFun_(geneSet)
}

# MCL
mcl_inflation = 2.0
mcl_gq = 0

mcl_ClusterFunName <- paste0("MCL",
                             "qg", gsub("\\.","_", mcl_gq),
                             "I",  gsub("\\.","_", mcl_inflation))
mcl_ClusterFun_ <- prepare.MCL.ClusterFun(FunCoup_g,
                                          edgeWeightName = "PFC",
                                          inflation = mcl_inflation, gq = mcl_gq,
                                          verbose = T)
mcl_ClusterFun <- function(dataEntry, geneSet) {
  mcl_ClusterFun_(geneSet)
}

# choose the name of the benchmark according to the parameters
prefix <- paste0(gsm_methodName, "-geneperm")

# initialise benchmark with settings from above
benchmark <- GEABenchmark(
  dataDir              = file.path(baseDir, "dataDir.negative-geneperm"),
  idType               = "ENSEMBL",
  idMapper             = plug("FunCoup", idMapper_netw),
  tableMapper          = plug("lowestPval", lowestPval),
  geneSetMethod        = plug(gsm_methodName, gsm_method)
)

# ------------------------------------------------------------------------------
# False positives
# ------------------------------------------------------------------------------

goodDataEntries  <- getDataEntries(benchmark, goodExperiments)

setRandomizer <- create.randomizeGeneSet(FA_network$netw_igraph, error = 0.05)

pQueryGeneSets_genePerm <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = setRandomizer,
  recompute = F, ncores = 3
)

# no clustering
# ..............................................................................

FPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3),
                              queries = pQueryGeneSets_genePerm,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3),
                              queries = pQueryGeneSets_genePerm,
                              targets = targetGeneSets,
                              prefix  = prefix)
  ,
  "BinoX"          = prepTool(fun     = getCBplugin(randNetPath, 2),
                              queries = pQueryGeneSets_genePerm,
                              targets = targetGeneSets,
                              prefix  = prefix)
)

runFPBenchmark(benchmark, FPsetup, goodDataEntries, ncores = 2)

# MGclus
# ..............................................................................

settings(benchmark) <- list(
  geneSetClusterMethod = plug(mgc_ClusterFunName, mgc_ClusterFun)
)

pmgc_clusteredGeneSets_geneperm <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "ClusteredGeneSets",
  ncores = 5
)

# remove modules of size one
pmgc_clusteredGeneSets_geneperm <- foreach(
  perm = pmgc_clusteredGeneSets_geneperm,
  .final = function(pmgc_clusteredGeneSets_filtered) {
    `names<-`(pmgc_clusteredGeneSets_filtered, names(pmgc_clusteredGeneSets_geneperm))
  }
) %:% foreach(
    mgc_geneset = perm,
    .final = function(mgc_filteredgenesets) {
        `names<-`(mgc_filteredgenesets, names(perm))
    }
  ) %:%
    foreach(
      mgc_module = mgc_geneset,
      .final = function(mgc_modules) {
        `names<-`(mgc_modules, names(mgc_geneset)[1:length(mgc_modules)])
      }
    ) %:% when(
      length(mgc_module) > 1
    ) %do% {
        mgc_module
    }

mgc_FPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3, clustered = TRUE),
                              queries = pmgc_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mgc_ClusterFunName))
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3, clustered = TRUE),
                              queries = pmgc_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mgc_ClusterFunName))
  ,
  "BinoX"          = prepTool(fun     = getCBplugin(
                                randNetPath, 2,
                                clustered = TRUE
                              ),
                              queries = pmgc_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mgc_ClusterFunName))
)

runFPBenchmark(benchmark, mgc_FPsetup, goodDataEntries, ncores = 3)

# MCL
# ..............................................................................

settings(benchmark) <- list(
  geneSetClusterMethod = plug(mcl_ClusterFunName, mcl_ClusterFun)
)

pmcl_clusteredGeneSets_geneperm <- getPermutations(
  benchmark, goodDataEntries,
  permutations = 1:10, what = "ClusteredGeneSets",
  ncores = 5
)

# remove modules of size one
pmcl_clusteredGeneSets_geneperm <- foreach(
  perm = pmcl_clusteredGeneSets_geneperm,
  .final = function(pmcl_clusteredGeneSets_filtered) {
    `names<-`(pmcl_clusteredGeneSets_filtered, names(pmcl_clusteredGeneSets_geneperm))
  }
) %:% foreach(
    mcl_geneset = perm,
    .final = function(mcl_filteredgenesets) {
        `names<-`(mcl_filteredgenesets, names(perm))
    }
  ) %:%
    foreach(
      mcl_module = mcl_geneset,
      .final = function(mcl_modules) {
        `names<-`(mcl_modules, names(mcl_geneset)[1:length(mcl_modules)])
      }
    ) %:% when(
      length(mcl_module) > 1
    ) %do% {
        mcl_module
    }

mcl_FPsetup <- list(
  "Fisher"         = prepTool(fun     = getFisherTool(23e3, clustered = TRUE),
                              queries = pmcl_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mcl_ClusterFunName))
  ,
  "Ease"           = prepTool(fun     = getEaseTool(23e3, clustered = TRUE),
                              queries = pmcl_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mcl_ClusterFunName))
  ,
  "BinoX"          = prepTool(fun     = getCBplugin(
                                randNetPath, 2,
                                clustered = TRUE
                              ),
                              queries = pmcl_clusteredGeneSets_geneperm,
                              targets = targetGeneSets,
                              prefix  = paste0(prefix, mcl_ClusterFunName))
)

runFPBenchmark(benchmark, mcl_FPsetup, goodDataEntries, ncores = 3)

# ------------------------------------------------------------------------------
# Make summary of results
# ------------------------------------------------------------------------------

FPsummary <- summarizeFPResults(benchmark, prefix, recorrect = T,
                                saveAs = paste0(prefix, "_negative"),
                                dataEntries = goodDataEntries)
FPsummary_mgc <- summarizeFPResults(benchmark, paste0(prefix, mgc_ClusterFunName),
                                    recorrect = T,
                                    saveAs = paste0(prefix, mgc_ClusterFunName,"_negative"),
                                    dataEntries = goodDataEntries)
FPsummary_mcl <- summarizeFPResults(benchmark, paste0(prefix, mcl_ClusterFunName),
                                    recorrect = T,
                                    saveAs = paste0(prefix, mcl_ClusterFunName, "_negative"),
                                    dataEntries = goodDataEntries)
FPsummary$prefix     <- as.factor("No Clustering")
FPsummary_mgc$prefix <- as.factor("MGclus")
FPsummary_mcl$prefix <- as.factor("Markov Clustering")
FPsummary_all <- rbind(FPsummary, FPsummary_mcl, FPsummary_mgc)
fpp <- FPplot_pretty(FPsummary_all,
                     sprintf("q-value cutoff: %.2f, FC cutoff: %.2f, minemum %.2f%%",
                             gsm_qvalcut,
                             gsm_foldchc,
                             gsm_minfrac * 100),
                     12)
fpp

if (F) {
  plot_dir <- file.path(baseDir, "plots")
  dir.create(plot_dir)

  save_plot(file.path(plot_dir, paste0("p_", prefix,".pdf")), fpp, base_width = 7, base_height = 7)
}
